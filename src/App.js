import './App.css';
import Builder from "./Components/Builder/Builder";

function App() {
  return (
    <div className="App">
      <Builder/>
    </div>
  );
}

export default App;
