import React from 'react';

const List = props => {
    return (
        <li onClick={props.show}>
            <a href="#">{props.items}</a>
        </li>
    );
};

export default List;