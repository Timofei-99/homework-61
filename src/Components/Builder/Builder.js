import React, {useEffect, useState} from 'react';
import axios from "axios";
import List from "../List/List";
import './Builder.css';
import Country from "../Country/Country";

const Builder = () => {
    const [countries, setCountries] = useState([]);
    const [alpha, setAlpha] = useState({});
    const [border, setBorder] = useState([]);
    const [show, setShow] = useState(false);
    const url = 'https://restcountries.eu/rest/v2/all?fields=name;alpha3Code';
    const nextUrl = 'https://restcountries.eu/rest/v2/alpha/';


    const info = async (code) => {
        const data = await axios.get(nextUrl + code);
        setAlpha(data.data);
        const bordersArray = data.data.borders;

        const array = [];

        bordersArray.forEach(key => {
            array.push(axios.get(nextUrl + key));
        });

        const readyPromise = await Promise.all(array);
        const newArray = [];
        readyPromise.forEach(key => {
            const names = key.data.name;
            newArray.push(names);
        })
        setBorder(newArray);
        setShow(true);
    }


    useEffect(() => {
        const getAxios = async () => {
            const get = await axios.get(url);
            const response = get.data;
            setCountries(response);
        }
        getAxios().catch(console.error);
    }, []);


    return (
        <>
            <div className='list'>
                <ol>
                    {countries.map((country) => (
                        <List
                            key={country.name}
                            items={country.name}
                            show={() => info(country.alpha3Code)}
                        />
                    ))}
                </ol>
            </div>
            <Country
                country={alpha.name}
                capital={alpha.capital}
                population={alpha.population}
                flag={alpha.flag}
                promise={border}
                show={show}
            />
        </>
    );
};

export default Builder;