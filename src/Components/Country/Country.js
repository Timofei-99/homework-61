import React from 'react';
import './Country.css';
import Borders from "../Borders/Borders";

const Country = ({country, capital, population, flag, promise, show}) => {


     return (
         show ? <div className='Country'>
             <div className='countryBlock'>
                 <div className="usualInfo">
                     <h2>{country}</h2>
                     <p><strong>Capital: </strong>{capital}</p>
                     <p><strong>Population: </strong>{population}</p>
                 </div>
                 <img style={{width: '100px', height: '80px'}} src={flag} alt=""/>
             </div>
             <h3>Borders:</h3>
             <ul>
                 {promise.map((p) => (
                     <Borders
                         key={p}
                         border={p}
                     />
                 ))}
             </ul>
         </div> : <h2>Select country</h2>
    );
};

export default Country;

