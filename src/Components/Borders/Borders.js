import React from 'react';

const Borders = (props) => {
    return (
        <li>
            {props.border}
        </li>
    );
};

export default Borders;